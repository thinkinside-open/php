#!/bin/bash

set -e

function setPhpOption() {
    local setting="$1"
    local value="$2"
    local conf_path="$3"

    if [ -z "$setting" -o -z "$value" ]; then
        echo >&2 "Usage: $FUNCNAME <setting> <value>"
        return 1
    fi

    echo >&2 "Setting PHP config ${setting}=${value}"
    sed -i -e "/\s*$setting\s*=/d" -e "\$a\\$setting=$value" "$conf_path"
}

function configurePhp() {
    local phpConf="/etc/php5/fpm/php.ini"
    echo >&2 "Configuring PHP ($phpConf)"
    printenv | perl -ne 'print "$1 $2\n" if m/^PHP_CONF_(\S+?)=(.*)/'| while read setting value; do
        setPhpOption "$setting" "$value" "$phpConf"
    done

    local phpFpmConf="/etc/php5/fpm/php-fpm.conf"
    echo >&2 "Configuring PHP FPM ($phpFpmConf)"
    printenv | perl -ne 'print "$1 $2\n" if m/^PHP_FPM_CONF_(\S+?)=(.*)/'| while read setting value; do
        setPhpOption "$setting" "$value" "$phpFpmConf"
    done

    local phpFpmPoolDir="/etc/php5/fpm/pool.d/"
    echo >&2 "Configuring PHP FPM pools"
    printenv | perl -ne 'print "$1 $2 $3\n" if m/^PHP_FPM_POOL_(\S+?)_CONF_(\S+?)=(.*)/'| while read pool setting value; do
        setPhpOption "$setting" "$value" "$phpFpmPoolDir/${pool}.conf"
    done

    local phpCliConf="/etc/php5/cli/php.ini"
    echo >&2 "Configuring PHP CLI ($phpConf)"
    printenv | perl -ne 'print "$1 $2\n" if m/^PHP_CLI_CONF_(\S+?)=(.*)/'| while read setting value; do
        setPhpOption "$setting" "$value" "$phpCliConf"
    done    
}


function configureApache() {
    echo >&2 "Configuring Apache"

    sed -i -e 's/ErrorLog .*/ErrorLog \/dev\/stderr/' /etc/apache2/apache2.conf

    printenv | perl -ne 'print "$1\n" if m/^APACHE_ENABLE_MOD_(.+?)=.*/'| while read mod; do
        echo >&2 "Enabling Apache mod [${mod}]"
        a2enmod $mod
    done
    
    cat > /etc/apache2/conf-enabled/php-fpm.conf <<EOF
<IfModule mod_fastcgi.c>
  AddType application/x-httpd-fastphp5 .php
  Action application/x-httpd-fastphp5 /php5-fcgi
  Alias /php5-fcgi /usr/lib/cgi-bin/php5-fcgi
  FastCgiExternalServer /usr/lib/cgi-bin/php5-fcgi -socket /var/run/php5-fpm.sock -pass-header Authorization $FASTCGI_OPTS

  <Directory /usr/lib/cgi-bin>
    Require all granted
  </Directory>
</IfModule>
EOF
    
}

function configureVirtualHosts() {
    printenv | perl -ne 'print "$1 $2\n" if m/^VHOST_(.+?)_SERVER_NAME=.*/'| while read vhost; do
        echo >&2 "Configuring VirtualHost [$vhost]"
        if [ ! -f "/etc/apache2/sites-enabled/vhost_$vhost.conf" ]; then
            local serverNameVar="VHOST_${vhost}_SERVER_NAME"
            if [ -z "${!serverNameVar}" ]; then
                echo >&2 "ServerName directive MUST be specified using env ${serverNameVar}"
                return 1
            fi
            echo >&2 " - ServerName=${!serverNameVar}"

            local serverAdminVar="VHOST_${vhost}_SERVER_ADMIN" 
            local SERVER_ADMIN_DIRECTIVE=""
            if [ -n "${!serverAdminVar}" ]; then
                echo >&2 " - ServerAdmin=${!serverAdminVar}"
                SERVER_ADMIN_DIRECTIVE="ServerAdmin ${!serverAdminVar}"
            fi

            local includeVar="VHOST_${vhost}_INCLUDE"
            local INCLUDE_DIRECTIVE=""
            if [ -n "${!includeVar}" ]; then
                echo >&2 " - Include=${!includeVar}"
                INCLUDE_DIRECTIVE="Include ${!includeVar}"
            fi

        cat > "/etc/apache2/sites-enabled/vhost_$vhost.conf" <<EOF
<VirtualHost *:80>
  ServerName ${!serverNameVar}
  ${SERVER_ADMIN_DIRECTIVE}

  <Directory /www/$vhost>
        Options -Indexes +FollowSymLinks
        AllowOverride All
        Require all granted
  </Directory>

  DocumentRoot /www/$vhost

  ErrorLog /dev/stderr
  CustomLog /dev/stdout combined

  ${INCLUDE_DIRECTIVE}
</VirtualHost>
EOF
            
        else
            echo >&2 "VirtualHost [$vhost] is already configured"
        fi
    done
}

function start_server() {
    # Starting php and apache2 in background
    php5-fpm &
    rm -f /var/run/apache2/apache2.pid && apache2ctl -D FOREGROUND &
    # start php-fpm_exporter
    /php-fpm_exporter server &
    # Wait for the any process to exit
    wait
    exit $?    
}

configurePhp
configureApache
configureVirtualHosts


case "$1" in
    server | "") start_server;;
    *) exec $*;;
esac
