# Base PHP image

The image is based on PHP 5.6 and Apache 2.4.

## Configuration

The image supports quite flexible configuration by a mixture of environment variables and file mounts.

### Configuring PHP

The PHP configuration can be tuned by using the environment varibale pattern `PHP_CONF_<key>=<value>`.

These configurations will be applied to the `/etc/php5/fpm/php.ini` file.

Similarly you can configure PHP FPM by using the pattern `PHP_FPM_CONF_<key>=<value>`.

These configuration will be applied to the `/etc/php5/fpm/php-fpm.conf` file.

To configure a specific PHP FPM pool setting use the pattern `PHP_FPM_POOL_<poolname>_CONF_<key>=<value>`

These configuration will be applied to the `/etc/php5/fpm/pool.d/<poolname>.conf` file.

Finially to configure PHP for CLI usage use the pattern `PHP_CLI_CONF_<key>=<value>`.

These configuration will be applied to the `/etc/php5/cli/php.ini` file.

### Configuring Apache

Global Apache configuration can be achieved by mounting a volume in the `/etc/apache2/conf-enabled/` directory.
This way the settings will be shared amongst all configured *VirtualHosts*.

Apache modules can be enabled by setting the environment variable `APACHE_ENABLE_MOD_<mod>=1`.

In order to specify parameters for the fastcgi plugin use the 
`FASTCGI_OPTS` variable.
Example: `FASTCGI_OPTS=-idle-timeout 600`
### Configuring VirtualHosts

To configure a *VirtualHost* it is sufficient to define the variable `VHOST_<id>_SERVER_NAME`. 
This will cause the generation of a skeleton *VirtualHost* configuration in the file `/etc/apache2/sites-enabled/vhost_<id>.conf`.
The generated *VirtualHost* will use as *DocumentRoot* the directory `/www/<id>`.

The other configurations that can be specified via environment variable for a given *VirtualHost* are:
* `VHOST_<id>_SERVER_ADMIN`: the address of the server admin (default *webmaster@localhost*)
* `VHOST_<id>_INCLUDE`: the path of an Apache configuration file to include in the *VirtualHost* definition

To manually configure a *VirtualHosts* it is sufficient to mount the relative configuration file in `/etc/apache2/sites-enabled`

## Paths

Here are some of the paths used by image:

* `/www/`: root directory for www data. The default VirtualHost lives in `/www/default`, generated VirtualHost `<id>` lives in `/www/<id>`
* `/etc/apache2/conf-enabled/`: configuration directory for global Apache settings
* `/etc/apache2/sites-enabled`: configuration directory for VirtualHost manual definitions
`
## Example usage

Simplest usage, using default *VirtualHost*:
```
docker run --rm -v /path/to/www/data:/www/default -p 8080:80 registry.gitlab.com/thinkinside-open/php
```

Definition of dedicated *VirtualHost* with included configuration:
```
docker run --rm -v /path/to/www/data:/www/test -v /path/to/test/conf:/test.conf -e VHOST_test_SERVER_NAME=test.io -e VHOST_test_INCLUDE=/test.conf -p 8080:80 registry.gitlab.com/thinkinside-open/php
```


