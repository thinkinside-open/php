FROM debian:jessie

RUN sed -ie '/^deb/s/main/main non-free/' /etc/apt/sources.list \
    && apt-get update \
    && apt-get -y --no-install-recommends install \
         apache2 \
         libapache2-mod-fastcgi \
         php5-fpm \
    && a2enmod actions \
    && a2enmod proxy \
    && a2enmod proxy_fcgi \
    && a2enmod proxy_http \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /usr/share/doc/*

RUN mkdir /www \
    && chown www-data /www \
    && rm -f /etc/apache2/sites-enabled/000-default.conf

ENV EXPORTER_VERSION=2.2.0
ADD https://github.com/hipages/php-fpm_exporter/releases/download/v${EXPORTER_VERSION}/php-fpm_exporter_${EXPORTER_VERSION}_linux_amd64 php-fpm_exporter
RUN chmod +x php-fpm_exporter

ENV PHP_FPM_SCRAPE_URI="unix:///var/run/php5-fpm.sock;/status"

ADD run.sh /run.sh

ENTRYPOINT ["/run.sh"]